<?php

/**
* Implements hook_entity_info_alter().
*/
function commerce_gallery_entity_info_alter(&$entity_info) {
  $entity_info['node']['view modes']['commerce_gallery'] = array(
    'label' => t('Commerce Gallery'),
    'custom settings' => TRUE,
  );
}

/**
 * Implements hook_menu().
 */
function commerce_gallery_menu() {
  $items = array();
  $items['commerce-gallery/%ctools_js/%node'] = array(
    'page callback' => 'commerce_gallery_gallery',
    'page arguments' => array(1, 2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function commerce_gallery_theme() {
  return array(
    'commerce_gallery_thumbnail' => array(
      'variables' => array(
        'product_images' => array(),
        'display_images' => array(),
        'settings' => array(),
        'plugin' => array(),
        'field_name' => NULL,
      ),
    ),

    'commerce_gallery' => array(
      'render element' => 'node',
    ),
    
    'commerce_gallery_table' => array(
      'variables' => array(
        'items' => array(),
        'columns' => NULL,
        'show' => NULL,
        'attributes' => array('class' => array('extra-images')),
      ),
    ),

    'commerce_gallery_product_image_classes' => array(
      'variables' => array(
        'field_name' => NULL,
        'empty' => NULL,
      ),
    ), 
  );
}

/**
 * Implements hook_ctools_plugin_type().
 */
function commerce_gallery_ctools_plugin_type() {
  return array(
    'style' => array(
      'load themes' => TRUE,
      'cache' => FALSE,
    ),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function commerce_gallery_ctools_plugin_directory($module, $plugin) {
  if ($module == 'commerce_gallery' && $plugin == 'style') {
    return 'styles';
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function commerce_gallery_field_formatter_info() {
  return array(
    'commerce_gallery' => array(
      'label' => t('Commerce Gallery'),
      'field types' => array('image'),
      'settings' => array(
        'thumbnail_image_style' => 'commerce_gallery_m',
        'gallery_image_style' => 'commerce_gallery_l',
        'thumbnail_extra_image_style' => 'commerce_gallery_xs',
        'gallery_extra_image_style' => 'commerce_gallery_s',
        'product_display_image_fields' => NULL,
        'style' => 'default',
      ),
      'description' => t('Combine images from several fields'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function commerce_gallery_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  
  $form = array();
  $instances = field_info_instances('node');

  $product_display = NULL;
  foreach ($instances as $bundle_name => $bundle) {
    foreach ($bundle as $field_name => $field_data) {
      $info = field_info_field($field_name);
      if ($info['type'] == 'commerce_product_reference') {
        if (!empty($instances[$bundle_name][$field_name]['settings']['referenceable_types'])) {
          $referenceable_types = $instances[$bundle_name][$field_name]['settings']['referenceable_types'];
          if ($filter = array_filter($referenceable_types)) {
            $referenceable_types = $filter;
          }
          if (isset($referenceable_types[$instance['bundle']])) {
            $product_display = $bundle_name;
            break;
          }
        }
      }
    }
  }
  
  if (!$product_display) {
    $form['product_display'] = array(
      '#markup' => t('Product displays for this product type not found.'),
    ); 
    return $form;
  }
  
  $form['product_display'] = array(
    '#markup' => '<br clear="all"/>' .
      t('Product display node type: @type', array('@type' => $product_display)),
  ); 
  
  $image_fields = array();
  if (!empty($instances[$product_display])) {
    foreach ($instances[$product_display] as $field_name => $field_data) {
      $info = field_info_field($field_name);
      
      if ($info['type'] == 'image') {
        $image_fields[$field_name] = $field_name;
      }
    }
  }

  $form['product_display_image_fields'] = array(
    '#title' => t('Image fields'),
    '#type' => 'checkboxes',
    '#prefix' => '<div id="product-display-image-fields">',
    '#suffix' => '</div>',
    '#options' => $image_fields,
    '#default_value' => $settings['product_display_image_fields'],
  );

  $options = array();
  foreach (image_styles() as $id => $style) {
    $options[$id] = $id;
  }
  
  $form['thumbnail_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail image style'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $settings['thumbnail_image_style'],
  );
  
  $form['thumbnail_extra_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail extra images style'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $settings['thumbnail_extra_image_style'],
  );
  
  $form['gallery_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Gallery image style'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $settings['gallery_image_style'],
  );
  
  $form['gallery_extra_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Gallery extra images style'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $settings['gallery_extra_image_style'],
  );
  
  ctools_include('plugins');
  $plugins = ctools_get_plugins('commerce_gallery', 'style');
  
  $options = array();
  foreach ($plugins as $plugin) {
    $options[$plugin['name']] = check_plain($plugin['title']);
  }
  
  $form['style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#options' => $options,
    '#default_value' => $settings['style'],
  );

  return $form;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function commerce_gallery_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary[] = '--';
  return implode ('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function commerce_gallery_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  $settings = $display['settings'];

  $images = array();
  if ($settings['product_display_image_fields']) {
    foreach ($settings['product_display_image_fields'] as $image_field) {
      if (!empty($entity->display_context['entity']->{$image_field}[LANGUAGE_NONE])) {
        $images[$image_field] = $entity->display_context['entity']->{$image_field}[LANGUAGE_NONE];
      }
    }
  }

  ctools_include('plugins');
  $plugin = ctools_get_plugins('commerce_gallery', 'style', $settings['style']);

  $variables = array(
    'product_images' => $items,
    'display_images' => $images,
    'settings' => $settings,
    'plugin' => $plugin,
    'entity' => $entity,
    'field_name' => $field['field_name'],
  );

  $element[0]['#markup'] = theme('commerce_gallery_thumbnail', $variables);
  return $element;
}

/**
 * Page callback. Returns popup content.
 */
function commerce_gallery_gallery($js, $node) {

    if ($js) {
      if (!empty($_GET['pfield']) && !empty($_GET['ptype'])) {
        // Convert underscores to hyphens
        $product_type = str_replace('-', '_', $_GET['ptype']);
        $product_field = str_replace('-', '_', $_GET['pfield']);
        
        // We know field name and product type - get field instance settings
        $field = field_info_instance('commerce_product', $product_field, $product_type);
        
        // Build renderable array of node content
        $langcode = $GLOBALS['language_content']->language;
        node_build_content($node, 'commerce_gallery', $langcode);

        $build = $node->content;
        unset($node->content);

        $build += array(
          // Our custom theme to display node content
          '#theme' => 'commerce_gallery', 
          '#node' => $node, 
          // Our custom view mode defined in hook_entity_info_alter()
          '#view_mode' => 'commerce_gallery',
          '#language' => $langcode,
          // Pass field settings to theme_commerce_gallery
          '#commerce_gallery_field' => $field,
        );
        
        // Include Ctools's stuff
        ctools_include('modal');
        ctools_include('ajax');
        $commands = array();
        // Render node structure
        $commands[] = ctools_modal_command_display('', $build);
        print ajax_render($commands);
        exit;
      }
    }
    // If JS disabled, redirect user back and set nojs flag
    else {
      return drupal_goto('node/' . $node->nid, array(
        'query' => array('gallery_nojs' => true)));
    }
  // Display 403 Error if all gone wrong
  return MENU_ACCESS_DENIED;
}

/**
 * Implements hook_image_default_styles().
 */
function commerce_gallery_image_default_styles() {
  $styles = array();
  $styles['commerce_gallery_xs'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 50,
          'height' => 50,
          'upscale' => 0,
        ),
        'weight' => -10,
      ),
    ),
  );
  
  $styles['commerce_gallery_s'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 80,
          'height' => 80,
          'upscale' => 0,
        ),
        'weight' => -10,
      ),
    ),
  );
  
  $styles['commerce_gallery_m'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => 300,
          'upscale' => 0,
        ),
        'weight' => -10,
      ),
    ),
  );
  
  $styles['commerce_gallery_l'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 400,
          'height' => 400,
          'upscale' => 0,
        ),
        'weight' => -10,
      ),
    ),
  );
  
  
  if (module_exists('imagecache_canvasactions')) {
    $styles['commerce_gallery_xs']['effects'][] = array(
      'name' => 'canvasactions_canvas2file',
      'data' => array(
        'xpos' => 'center',
        'ypos' => 'center',
        'alpha' => '100',
        'path' => 'module://commerce_gallery/images/XS.png',
        'dimensions' => 'background',
      ),
      'weight' => 0,
    );
      
    $styles['commerce_gallery_s']['effects'][] = array(
      'name' => 'canvasactions_canvas2file',
      'data' => array(
        'xpos' => 'center',
        'ypos' => 'center',
        'alpha' => '100',
        'path' => 'module://commerce_gallery/images/S.png',
        'dimensions' => 'background',
      ),
      'weight' => 0,
    );
      
    $styles['commerce_gallery_m']['effects'][] = array(
      'name' => 'canvasactions_canvas2file',
      'data' => array(
        'xpos' => 'center',
        'ypos' => 'center',
        'alpha' => '100',
        'path' => 'module://commerce_gallery/images/M.png',
        'dimensions' => 'background',
      ),
      'weight' => 0,
    );
    
    $styles['commerce_gallery_l']['effects'][] = array(
      'name' => 'canvasactions_canvas2file',
      'data' => array(
        'xpos' => 'center',
        'ypos' => 'center',
        'alpha' => '100',
        'path' => 'module://commerce_gallery/images/L.png',
        'dimensions' => 'background',
      ),
      'weight' => 0,
    );
  }
  return $styles;
}

/**
 * Theme function. Returns html of table with extra thumbnails.
 */
function theme_commerce_gallery_table($variables) {
  $rows = array();
  $row_indexes = array();
  $table_id = $variables['attributes']['id'];
  if ($variables['show']) {
    // Include small js that allows to hide remaining rows after specific threshold
    $show = $variables['show'] - 1; // Jquery counts table rows starting from 0
    $js = '
      (function ($) {
        Drupal.behaviors.toggleThumbRows = {
          attach: function (context, settings) {
            var rowCount = $("table#' .  $table_id . ' tr").length;
            if (rowCount > ' . $show . ') {
              $("a.more-thumbs").show();
            }
          
            $("table#' .  $table_id . ' tr:gt(' . $show . ')").hide();
            $("a.more-thumbs").click(function (){
              var txt = $("table#' . $table_id . ' tr:gt(' . $show . ')").is(\':visible\') ? "' . t('Show more') . '" : "' . t('Show less') . '";
              $("a.more-thumbs").text(txt);
              $("table#' . $table_id . ' tr:gt(' . $show . ')").toggle();
            });
          }
        };
      })(jQuery);
    ';
    drupal_add_js($js, 'inline');
  }
  
  $items = $variables['items'];
  $columns = $variables['columns'];

  $num_rows = floor(count($items) / $columns);
  $remainders = count($items) % $columns;
  $row = 0;
  $col = 0;

  foreach ($items as $count => $item) {
    $rows[$row][$col] = $item;
    $row_indexes[$row][$col] = $count;
    $row++;

    if (!$remainders && $row == $num_rows) {
      $row = 0;
      $col++;
    }
    elseif ($remainders && $row == $num_rows + 1) {
      $row = 0;
      $col++;
      $remainders--;
    }
  }
  
  for ($i = 0; $i < count($rows[0]); $i++) {
    if (!isset($rows[count($rows) - 1][$i])) {
      $rows[count($rows) - 1][$i] = '';
    }
  }
  
  $output = theme('table', array(
    'rows' => $rows,
    'attributes' => $variables['attributes'],
  ));
  
  // Add "Show more" link
  if ($variables['show']) {
    $output .= '<a class="more-thumbs" style="display:none;">' . t('Show more') . '</a>';
  }
  return $output;
}

/**
 * Theme fucntion. Returns html of preview content (thumbnail, extra images).
 */
function theme_commerce_gallery_thumbnail($variables) {
  $output = '';
  $plugin = $variables['plugin'];
  $vars['gallery'] = $variables;

  if (isset($plugin['callback']) && function_exists($plugin['callback'])) {
    $plugin['callback']($vars, 'thumbnail');
  }

  $template = $plugin['path'] . '/' . $plugin['name'] . '--thumbnail.tpl.php';
  if (file_exists($template)) {
    // Render template
    $output = theme_render_template($template, $vars);
  }
  return $output;
}

/**
 * Theme function. Returns html for gallery inside popup window.
 */
function theme_commerce_gallery($variables) {
  $output = '';
  $settings = $variables['node']['#commerce_gallery_field']['display']['default']['settings'];
  $plugin_name = $settings['style'];
  // Load plugin of style to get more information
  $plugin = commerce_gallery_plugin_load($plugin_name);
  
  // Pass plugin settings to plugin's callback function and template
  $variables['gallery']['settings'] = $settings;
  if (isset($plugin['callback']) && function_exists($plugin['callback'])) {
    // Invoke style's callback function
    $plugin['callback']($variables, 'gallery');
  }

  $template = $plugin['path'] . '/' . $plugin['name'] . '--gallery.tpl.php';
  if (file_exists($template)) {
    // Render template
    $output = theme_render_template($template, $variables);
  }
  else {
    $output = drupal_render_children($variables['node']);
  }
  return $output;
}

/**
 * Theme function. Returns wrapper classes to refresh product image upon changing options in the "Add to cart" form
 */
function theme_commerce_gallery_product_image_classes($variables) {
  $classes = array(
    'commerce-product-field',
    drupal_html_class('commerce-product-field-' . $variables['field_name']),
    drupal_html_class('field-' . $variables['field_name']),
  );

  if ($variables['empty']) {
    $classes[] = 'commerce-product-field-empty';
  }
  return implode(' ', $classes);
}

/**
 * Load plugin by its ID
 */
function commerce_gallery_plugin_load($id) {
  ctools_include('plugins');
  return ctools_get_plugins('commerce_gallery', 'style', $id);
}