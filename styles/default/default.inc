<?php

$plugin = array(
  'title' => t('Default'),
  'callback' => '_commerce_gallery_style_default',
);

/**
 * Main processing function of default style
 */
function _commerce_gallery_style_default(&$vars, $type) {
  
  extract($vars['gallery']['settings']);

  $zoom_ratio_width = 1.5;
  $zoom_ratio_height = 1.5;
  
  $vars['gallery']['product_first_image_original_url'] = '';
  $vars['gallery']['product_first_image_src'] = '';
  $vars['gallery']['gallery_popup_href'] = '';
  $vars['gallery']['product_image_classes'] = '';
  $vars['gallery']['extra_images_gallery'] = '';
  $vars['gallery']['extra_images_thumbnail'] = '';
  $vars['gallery']['product_first_image_original_url_control'] = ' ';
  
  $path = drupal_get_path('module', 'commerce_gallery') . '/styles/default';
  
  if ($type == 'thumbnail') {
    
    _commerce_gallery_style_files();
  
    $nid = $vars['gallery']['entity']->display_context['entity']->nid;
    $url = url('commerce-gallery/nojs/'. $nid, array(
      'query' => array(
        'pfield' => str_replace('_', '-', $vars['gallery']['field_name']),
        'ptype' => str_replace('_', '-', $vars['gallery']['entity']->type),  
      )));
      
    $vars['gallery']['gallery_popup_href'] = $url;
    
    $vars['gallery']['gallery_popup_icon'] = theme('image', array(
      'path' => $path . '/images/images.png',
      'alt' => t('Show images'),
      'title' => t('Show images')));
      
    $vars['gallery']['gallery_popup_icon_link'] = '<a href="' . $url . '" class="icon ctools-use-modal ctools-modal-commerce-gallery-default">' .
      $vars['gallery']['gallery_popup_icon'] . '</a>';
    
    $vars['gallery']['product_image_classes'] = theme('commerce_gallery_product_image_classes', array(
      'field_name' => $vars['gallery']['field_name']));
  
    $product_images_source = $vars['gallery']['product_images'];
    $display_images_source = $vars['gallery']['display_images'];
  }
  elseif ($type == 'gallery') {
    $vars['gallery']['title'] = check_plain($vars['node']['#node']->title);
    $field_name = $vars['node']['#commerce_gallery_field']['field_name'];
    $product_images_source = $vars['node']['product:' . $field_name]['#items'];

    $display_images_source = array();
    if ($product_display_image_fields) {
      foreach ($product_display_image_fields as $image_field) {
        if (!empty($vars['node'][$image_field]['#items'])) {
          $display_images_source[$image_field] = $vars['node'][$image_field]['#items'];
        }
      }
    }
    
    $thumbnail_image_style = $gallery_image_style;
    $thumbnail_extra_image_style = $gallery_extra_image_style;
  }

  if ($product_images_source) {
    $first_product_key = key($product_images_source);
  }
  
  $flatten_images = call_user_func_array('array_merge', $display_images_source);

  if ($merged_images = array_merge($product_images_source, $flatten_images)) {
    $images = array();
    foreach ($merged_images as $key => $image) {
      $images[$type][$key]['url'] = file_create_url($image['uri']);
      $images[$type][$key]['extra_images_url'] = image_style_url($thumbnail_extra_image_style, $image['uri']);
      
      $images[$type][$key]['thumbnail_uri'] = image_style_path($thumbnail_image_style, $image['uri']);
      $images[$type][$key]['thumbnail_url'] = file_create_url($images[$type][$key]['thumbnail_uri']);
      
      $image_info = image_get_info($image['uri']);
      $thumbnail_info = image_get_info($images[$type][$key]['thumbnail_uri']);
      
      $images[$type][$key]['zoomable'] = FALSE;
      if (!empty($image_info) && !empty($thumbnail_info)) {
        if (($image_info['width'] / $thumbnail_info['width']) > $zoom_ratio_width ||
          ($image_info['height'] / $thumbnail_info['height']) > $zoom_ratio_height) {
            $images[$type][$key]['zoomable'] = TRUE;
        }
      }
    }
  
    $i = 1;
    $output_images = array();
    foreach ($images[$type] as $key => $image) {
      
      $config = array(
        'useZoom' => '#zoom' . $type,
        'image' => $images[$type][$key]['thumbnail_url'],
        'zoomImage' => $images[$type][$key]['zoomable'] ? $images[$type][$key]['url'] : ' ',
      );
      
      $title = !empty($image['title']) ? check_plain($image['title']) : t('Image @num', array(
        '@num' => $i));
      
      $pic = theme('image', array(
        'path' => $images[$type][$key]['extra_images_url'],
        'title' => $title,
        'alt' => $title,
      ));

      $output_images[$type][$key] = "<a data-cloudzoom='" . json_encode($config) . "' class='cloudzoom-gallery' target='_blank' href='" . $images[$type][$key]['url'] . "'>$pic</a>";
      $i++;
    }
    
    if (isset($first_product_key)) {
      $vars['gallery']['product_first_image_original_url_control'] = $images[$type][$first_product_key]['zoomable'] ? $images[$type][$first_product_key]['url'] : ' ';
      $vars['gallery']['product_first_image_original_url'] = $images[$type][$first_product_key]['url'];
      $vars['gallery']['product_first_image_src'] = $images[$type][$first_product_key]['thumbnail_url'];
    }

    if (!empty($output_images['thumbnail'])) {
      $vars['gallery']['extra_images_thumbnail'] = theme('commerce_gallery_table', array(
        'items' => $output_images['thumbnail'],
        'columns' => 5,
        'show' => 1,
        'attributes' => array(
          'id' => 'commerce-gallery-thumplains',
          'class' => array('extra-images'),
        )));
    }
    
    if (!empty($output_images['gallery'])) {
      $vars['gallery']['extra_images_gallery'] = theme('commerce_gallery_table', array(
        'items' => $output_images['gallery'],
        'columns' => 4,
        'attributes' => array(
          'id' => 'commerce-gallery',
          'class' => array('extra-images'),
          )));
    }
  }
}


function _commerce_gallery_style_files() {
  
  $path = drupal_get_path('module', 'commerce_gallery') . '/styles/default';
  $config_cloudzoom = '
    Drupal.behaviors.cloudZoomGallery = {
      attach: function (context, settings) {
        CloudZoom.quickStart();
      }
    };
  ';
  
  $close = theme('image', array(
    'path' => $path . '/images/close.png',
    'alt' => t('Close window'),
    'title' => t('Close window')));
    
  $throbber = theme('image', array(
    'path' => $path . '/images/spinner.png',
    'alt' => t('Loading...'),
    'title' => t('Loading...')));
  
  $custom_style = array(
    'commerce-gallery-default' => array(
      'modalSize' => array(
        'type' => 'scale',
        'width' => .8,
        'height' => .8,
        'contentRight' => 0,
        'contentBottom' => 0,
      ),
      'modalOptions' => array(
        'opacity' => 0.5,
        'background' => '#000',
      ),
      'closeImage' => $close,
      'throbber' => $throbber,
      'animation' => 'fadeIn',
      'modalTheme' => 'ModalCommerceGalleryDefault',
    ),
  );
  
  drupal_add_js($path . '/cloudzoom/cloudzoom.js');
  drupal_add_js($config_cloudzoom, 'inline');
  
  drupal_add_css($path . '/cloudzoom/cloudzoom.css');
  drupal_add_css($path . '/css/default.css');
  
  drupal_add_js($custom_style, 'setting');
  drupal_add_js($path . '/js/popup.js');
}


