  Drupal.theme.prototype.ModalCommerceGalleryDefault = function () {
    var html = ''
    html += '  <div id="ctools-modal">'
    html += '    <div class="ctools-modal-content commerce-gallery">'
    html += '        <a class="close" href="#">';
    html +=            Drupal.CTools.Modal.currentSettings.closeImage;
    html += '        </a>';
    html += '      <div id="modal-content" class="modal-content">';
    html += '      </div>';
    html += '    </div>';
    html += '  </div>';

    return html;
  }