<table class="container">
  <tr>
    <td class="left">
      <img id="zoomgallery" class="cloudzoom" src="<?php print $gallery['product_first_image_src']; ?>" data-cloudzoom = "zoomImage: '<?php print $gallery['product_first_image_original_url'];?>', zoomPosition: 'inside'" />
    </td>
    <td class="right">
      <h4 class="title"><?php print $gallery['title']; ?></h4>
      <div id="thumbs"><?php print $gallery['extra_images_gallery']; ?></div> 
    </td>
  </tr>
</table>

<?php
  /*
  // See what available
  // Get gallery array (see default.inc)
  print_r($gallery);
  
  // Get node object
  print_r($node);
  */
?>

